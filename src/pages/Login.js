import { useState, useContext, useEffect } from "react";
import { Form, Button, Container } from "react-bootstrap";

import Swal from "sweetalert2";

import UserContext from "../UserContext";

//import Navigate component from react-router-dom, this will allow us to Redirect our users after logging in and updating the global user state. If a logged in user tries to go to our login page via browser, they will be redirected to our home page.
import { Navigate } from "react-router-dom";
function Login() {
    const { user, setUser } = useContext(UserContext);

    //input states
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    //state for conditional rendering our button
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        const conditions = [email !== "", password !== ""];
        conditions.every((element) => element) ? setIsActive(true) : setIsActive(false);
    }, [email, password]);

    const login = (e) => {
        e.preventDefault();
        fetch("http://localhost:4000/users/login", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                if (data.accessToken) {
                    Swal.fire({
                        icon: "success",
                        title: "Login Successful",
                        text: "Thank you for logging in",
                    });
                    //save our accessToken when we log in successfully. We will save it in our localStorage.
                    //localStorage is an object in JS which allows us to save small amounts of data within our browser. We can use this to save our token.
                    //localStorage exists in most browsers.
                    //localStorage.setItem() will allow us to save data in our browsers. However, any data we pass to localStorage will become a string.
                    //syntax: localStorage.setItem(<key>,<value>)

                    localStorage.setItem("token", data.accessToken);

                    //localStorage.getItem(<token>) - allows us to get the data of the key we will pass from our localStorage
                    let token = localStorage.getItem("token");

                    //use fetch() method to create a request to get our user details
                    fetch("http://localhost:4000/users/getUserDetails", {
                        method: "get",
                        headers: { Authorization: "Bearer " + token },
                    })
                        .then((res) => res.json())
                        .then((data) => {
                            console.log(data);
                            setUser({ id: data._id, email: data.email, isAdmin: data.isAdmin });
                        });
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Login Failed",
                        text: data.message,
                    });
                }
            });
    };

    //create a ternary to redirect our user if they are logged in. If not, we will show our form.
    return user.id ? (
        <Navigate to="/courses" replace={true} />
    ) : (
        <>
            <h1 className="my-5 text-center">Login</h1>
            <Form onSubmit={(e) => login(e)}>
                <Form.Group>
                    <Form.Label>Email:</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter Email"
                        required
                        value={email}
                        onChange={(e) => {
                            setEmail(e.target.value);
                        }}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter Password"
                        required
                        value={password}
                        onChange={(e) => {
                            setPassword(e.target.value);
                        }}
                    />
                </Form.Group>
                {isActive ? (
                    <Button variant="primary" type="submit" className="my-5">
                        Submit
                    </Button>
                ) : (
                    <Button variant="primary" disabled className="my-5">
                        Submit
                    </Button>
                )}
            </Form>
        </>
    );
}
export default Login;
